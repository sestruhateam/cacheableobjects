# Text task

*Create a configurable two-level cache (for caching Objects). Level 1 is memory, level 2 is filesystem. Config params should let one specify the cache strategies and max sizes of level  1 and 2.*


## Possible ways to decide

### Structure

 * [Inversion of Control](https://en.wikipedia.org/wiki/Inversion_of_control)
 * [Aspect-oriented programming](https://en.wikipedia.org/wiki/Aspect-oriented_programming)
 * Classic - over interfaces

### Possible cache features

 * Released [Algorithm caching](https://en.wikipedia.org/wiki/Cache_replacement_policies)
 * Validation cache by time

### Possible other features

 * Multithreading
 
 We choose classic way decide - [by interface](https://bitbucket.org/sestruhateam/cacheableobjects/src/master/src/main/java/ru/CacheableObjects/cache/Cache.java). To provide you with an example we will getting list [employees](https://bitbucket.org/sestruhateam/cacheableobjects/src/master/src/main/java/ru/CacheableObjects/models/Employee.java) and caching these query.
 The project is not yet ended and needs to be implemented the following subsystems:
 
 * two-level cache based on [File system cache](https://bitbucket.org/sestruhateam/cacheableobjects/src/master/src/main/java/ru/CacheableObjects/cache/FsCache.java) and [Memory cache](https://bitbucket.org/sestruhateam/cacheableobjects/src/master/src/main/java/ru/CacheableObjects/cache/MemCache.java)
 * behavior strategy between cache levels
 * cache strategy behavior
 * added javadoc
 * added automatic tests
 
 
package ru.CacheableObjects.cache;

import java.io.*;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class FsCache extends Cache {

    private final Map<String, ObjectWrapWithCounter> data = new HashMap<>(getMaxSize());
    private static final String DEFAULT_FOLDER = "fs-cache";

    private String cacheFolder;

    public FsCache() {
        this(DEFAULT_FOLDER);
    }

    public FsCache(String cacheFolder) {
        this.cacheFolder = cacheFolder;
        File dir = new File(this.cacheFolder);
        if (!dir.exists()) {
            dir.mkdir();
        }
    }

    @Override
    public boolean saveObject(String key, Object o) {
        try {
            if (data.size() + 1 >= getMaxSize()) {
                data
                        .values()
                        .stream()
                        .min(
                                Comparator.comparing(
                                        ObjectWrapWithCounter::getNumberOfUses))
                        .ifPresent(
                                objectWrapWithCounter ->
                                        data.remove(objectWrapWithCounter.getKey()));
            }

            String value = writeToFs(o);
            data.put(key, new ObjectWrapWithCounter(value, key));
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    @Override
    public Object getObjectFromKey(String key) {
        try {
            ObjectWrapWithCounter objectWrapWithCounter = data.get(key);
            if (objectWrapWithCounter == null) {
                return null;
            }
            objectWrapWithCounter.use();

            return readFromFs(objectWrapWithCounter.getObject().toString());
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    //TODO: check of exist with random UUID
    private String writeToFs(Object o) throws IOException {
        File file = new File(cacheFolder + File.separator + UUID.randomUUID().toString());
        file.createNewFile();
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(file))) {

            objectOutputStream.writeObject(o);

            return file.getPath();
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private Object readFromFs(String path) {
        File file = new File(path);
        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(file))) {
            return objectInputStream.readObject();
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
}

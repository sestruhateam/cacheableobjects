package ru.CacheableObjects.cache;

public class ObjectWrapWithCounter {
    private Integer numberOfUses = 0;
    private String key;
    private Object o;

    public ObjectWrapWithCounter(Object o, String key) {
        this.o = o;
        this.key = key;
    }

    public void use() {
        numberOfUses++;
    }

    public Integer getNumberOfUses() {
        return numberOfUses;
    }

    public String getKey() {
        return key;
    }

    public Object getObject() {
        return o;
    }
}

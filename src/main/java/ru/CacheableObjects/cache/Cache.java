package ru.CacheableObjects.cache;

public abstract class Cache {

    private static final int DEFAULT_MAX_SIZE = 10;

    private int maxSize = DEFAULT_MAX_SIZE;

    public abstract boolean saveObject(String key, Object o);

    public abstract Object getObjectFromKey(String key);

    public void setMaxSize(int maxSize) {
        this.maxSize = maxSize;
    }

    public int getMaxSize() {
        return maxSize;
    }
}

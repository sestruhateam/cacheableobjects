package ru.CacheableObjects.cache;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class MemCache extends Cache {

    private final Map<String, ObjectWrapWithCounter> data = new HashMap<>(getMaxSize());

    @Override
    public boolean saveObject(String key, Object o) {
        try {
            if (data.size() + 1 >= getMaxSize()) {
                data
                        .values()
                        .stream()
                        .min(
                                Comparator.comparing(
                                        ObjectWrapWithCounter::getNumberOfUses))
                        .ifPresent(
                                objectWrapWithCounter ->
                                        data.remove(objectWrapWithCounter.getKey()));
            }

            data.put(key, new ObjectWrapWithCounter(o, key));
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    @Override
    public Object getObjectFromKey(String key) {
        try {
            ObjectWrapWithCounter objectWrapWithCounter = data.get(key);
            if (objectWrapWithCounter == null) {
                return null;

            }
            objectWrapWithCounter.use();
            return objectWrapWithCounter.getObject();
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
}

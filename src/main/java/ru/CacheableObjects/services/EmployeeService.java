package ru.CacheableObjects.services;

import com.fasterxml.jackson.core.type.TypeReference;
import ru.CacheableObjects.cache.Cache;
import ru.CacheableObjects.models.Employee;
import ru.CacheableObjects.utils.JSON;
import ru.CacheableObjects.utils.http.Client;

import java.io.IOException;
import java.util.List;

public class EmployeeService {

    private static final String URL_GET_ALL_EMPLOYEES = "http://213.87.96.9:9897/getAllEmployees";
    private static final String URL_GET_EVEN_EMPLOYEES = "http://213.87.96.9:9897/getEvenEmployees";
    private static final String URL_GET_UNEVEN_EMPLOYEES = "http://213.87.96.9:9897/getUnevenEmployees";

    private final Cache cache;

    public EmployeeService(Cache cache) {
        this.cache = cache;
    }

    public String getAllEmployees() {
        return getEmployeesByQuery(URL_GET_ALL_EMPLOYEES);
    }

    public String getEvenEmployees() {
        return getEmployeesByQuery(URL_GET_EVEN_EMPLOYEES);
    }

    public String getUnevenEmployees() {
        return getEmployeesByQuery(URL_GET_UNEVEN_EMPLOYEES);
    }

    private String getEmployeesByQuery(String query) {
        List<Employee> employees = (List<Employee>) cache.getObjectFromKey(query);

        if (employees != null) {
            return employees.stream().map(Employee::toString).toString();
        }

        String jsonEmployees = Client.get(query).getResponseData();
        try {
            employees = JSON.getMapper().readValue(jsonEmployees, new TypeReference<List<Employee>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        cache.saveObject(query, employees);

        return employees.stream().map(Employee::toString).toString();
    }
}

package ru.CacheableObjects.utils.http;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Client {

    public static Response get(String url) {
        HttpGet httpGet = new HttpGet(url);

        return parseResponse(httpGet);
    }

    public static Response post(String url, List<Header> headers, BasicNameValuePair... params) throws IOException {

        HttpPost httpPost = new HttpPost(url);

        if (params.length > 0) {
            List<NameValuePair> bodyParams = new ArrayList<>();
            Collections.addAll(bodyParams, params);
            httpPost.setEntity(new UrlEncodedFormEntity(bodyParams));
        }

        if (headers != null) {
            headers.forEach(httpPost::addHeader);
        }

        return parseResponse(httpPost);
    }

    private static Response parseResponse(HttpUriRequest request) {

        try (CloseableHttpClient httpclient = HttpClients.createDefault()) {

            Response response = new Response();

            CloseableHttpResponse closeableHttpResponse = httpclient.execute(request);

            for (Header header : closeableHttpResponse.getAllHeaders()) {
                response.getHeaders().add(header);
            }

            response.setStatusLine(closeableHttpResponse.getStatusLine());


            HttpEntity entity = closeableHttpResponse.getEntity();
            StringBuilder stringBuilder = new StringBuilder();
            InputStream dataStream = entity.getContent();
            int symbol;

            while ((symbol = dataStream.read()) != -1) {
                stringBuilder.append((char) symbol);
            }

            response.setResponseData(stringBuilder.toString());
            return response;

        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
}

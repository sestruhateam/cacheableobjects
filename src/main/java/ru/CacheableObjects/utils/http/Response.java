package ru.CacheableObjects.utils.http;

import org.apache.http.Header;
import org.apache.http.StatusLine;

import java.util.ArrayList;
import java.util.List;

public class Response {
    private StatusLine statusLine;
    private String responseData;
    private final List<Header> headers = new ArrayList<>();

    public Response() {

    }

    public StatusLine getStatusLine() {
        return statusLine;
    }

    public void setStatusLine(StatusLine statusLine) {
        this.statusLine = statusLine;
    }

    public String getResponseData() {
        return responseData;
    }

    public void setResponseData(String responseData) {
        this.responseData = responseData;
    }

    public List<Header> getHeaders() {
        return headers;
    }
}

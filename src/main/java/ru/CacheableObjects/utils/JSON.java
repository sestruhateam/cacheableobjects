package ru.CacheableObjects.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JSON {
    private static final ObjectMapper mapper;

    static {
        mapper = new ObjectMapper();
    }

    public static ObjectMapper getMapper() {
        return mapper;
    }

    public static String objectToString(Object o) throws JsonProcessingException {
        return getMapper().writeValueAsString(o);
    }
}

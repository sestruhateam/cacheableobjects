package ru.CacheableObjects.models;

import java.io.Serializable;

public class Employee implements Serializable {
    private static final long serialVersionUID = 8620111838719438978L;
    private String name;
    private String rank;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    @Override
    public String toString() {
        return name + " ; " + rank;
    }
}
